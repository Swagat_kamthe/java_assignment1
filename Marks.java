import java.util.Scanner;
import sy.SY;
import ty.TY;

public class Marks{
	public String name;
	public int id;
	private int avg;
	
	public Marks(int id,String name)
	{
				
		this.id=id;
		this.name=name;
		//this.sy1= new SY(CT,MT,ET);
		//this.ty1= new TY(P,T);	
		
	}
	static void percentage(Marks std[],SY sy_std[],TY ty_std[],int n)
	{
		for(int i=0;i<n;i++)
		{
			std[i].avg=(sy_std[i].ComputerTotal+sy_std[i].MathsTotal+sy_std[i].ElectronicsTotal+ty_std[i].Theory+ty_std[i].Practicals)/5;
		}
		for(int i=0;i<n;i++)
		{
			System.out.println("\n\nID: "+std[i].id+"\nName : "+std[i].name+"\nPercentage: "+std[i].avg);

			if(std[i].avg>=70)
				System.out.println("Grade : A");
			else if(std[i].avg>=60)
				System.out.println("Grade : B");
			else if(std[i].avg>=50)
				System.out.println("Grade : C");
			else if(std[i].avg>=40)
				System.out.println("Grade : PASS");
			else 
				System.out.println("Grade : FAIL");
		}	
	}
	public static void main(String []args)
	{try{
	String name;
	int id;
	int CT,MT,ET,T,P;
	
	Scanner sc=new Scanner(System.in);
	
	System.out.println("Enter number of records : ");
	int n=sc.nextInt();
	Marks std[]=new Marks[n];
	SY sy_std[]=new SY[n];
	TY ty_std[]=new TY[n];
	for(int i=0;i<n;i++)
	{
		System.out.println("Enter Id : ");
		id=sc.nextInt();
		System.out.println("Enter name : ");
		name=sc.next();
		System.out.println("Enter CT : ");
		CT=sc.nextInt();
		System.out.println("Enter MT : ");
		MT=sc.nextInt();
		System.out.println("Enter ET : ");
		ET=sc.nextInt();
		System.out.println("Enter P : ");
		P=sc.nextInt();
		System.out.println("Enter T : ");
		T=sc.nextInt();	
		System.out.println();
		System.out.println();
		sy_std[i]= new SY(CT,MT,ET);
		ty_std[i]= new TY(P,T);
		std[i]=new Marks(id,name);

	}percentage(std,sy_std,ty_std,n);
}catch(Exception e)
{
		
}
}

}
